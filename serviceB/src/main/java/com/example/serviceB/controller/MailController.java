package com.example.serviceB.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mails")
public class MailController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailController.class);

    @PostMapping
    public void sendMail(@RequestBody String mailText) {
        LOGGER.info("SERVICE B - Received mailText: {}", mailText);
    }

}
