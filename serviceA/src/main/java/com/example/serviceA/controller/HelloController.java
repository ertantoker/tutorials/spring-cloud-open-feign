package com.example.serviceA.controller;

import com.example.serviceA.clients.MailServiceClient;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
@AllArgsConstructor
public class HelloController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloController.class);

    private final MailServiceClient mailServiceClient;

    @GetMapping
    public void sayHello(@RequestParam String message) {
        LOGGER.info("SERVICE A - Received message: {}", message);
        mailServiceClient.sendMail(message);
    }
}
