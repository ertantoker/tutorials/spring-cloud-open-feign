package com.example.serviceA.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "MaiLServiceClient", url = "http://localhost:8081")
public interface MailServiceClient {

    @PostMapping("/mails")
    void sendMail(@RequestBody String mailText);


}
